# Infracode Homelab
Infrastructure Code for my personal lab environment.

## Packer
The Packer tool is used to build Virtual Machine images which can be used by
Libvirt.

### Change password
The password are set in the following files:
  - ./centos8/builder.json
  - ./centos8/http/centos8-ks.cfg

## Example
Start building Virtual Machine images:
```
$ ./packer-build.sh 
[Building centos8/]
Downloading ISO image..
qemu: output will be in this color.

==> qemu: Retrieving ISO
==> qemu: Trying ./iso/CentOS-8.1.1911-x86_64-dvd1.iso
==> qemu: Trying ./iso/CentOS-8.1.1911-x86_64-dvd1.iso?checksum=sha256%3A3ee3f4ea1538e026fff763e2b284a6f20b259d91d1ad5688f5783a67d279423b
==> qemu: ./iso/CentOS-8.1.1911-x86_64-dvd1.iso?checksum=sha256%3A3ee3f4ea1538e026fff763e2b284a6f20b259d91d1ad5688f5783a67d279423b => /home/kees/code/packer/centos8/iso/CentOS-8.1.1911-x86_64-dvd1.iso
==> qemu: Creating required virtual machine disks
==> qemu: Starting HTTP server on port 8336
==> qemu: Found port for communicator (SSH, WinRM, etc): 3638.
==> qemu: Looking for available port between 5900 and 6000 on 127.0.0.1
==> qemu: Starting VM, booting from CD-ROM
    qemu: The VM will be run headless, without a GUI. If you want to
    qemu: view the screen of the VM, connect via VNC without a password to
    qemu: vnc://127.0.0.1:5998
==> qemu: Overriding defaults Qemu arguments with QemuArgs...
==> qemu: Waiting 10s for boot...
==> qemu: Connecting to VM via VNC (127.0.0.1:5998)
==> qemu: Typing the boot command over VNC...
==> qemu: Using ssh communicator to connect: 127.0.0.1
==> qemu: Waiting for SSH to become available...
```

