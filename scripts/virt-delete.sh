#!/bin/bash

inventory="../ansible/inventory.ini"

if [[ -z $1 ]]; then
  cat ${inventory} \
    | awk -F' ' '{print $1}'
  exit 1
fi

###############################
# Shutting down each host
###############################
for vhost in $@; do
  if [[ -n `virsh list | grep ${vhost}` ]]; then
    virsh shutdown ${vhost} || exit 1
  fi
done

###############################
# Waiting for shutdown
###############################
while true; do
  running=0
  for vhost in $@; do
    printf '.'
    if [[ -n `virsh list | grep ${vhost}` ]]; then
      running=1
    fi
  done
  [[ ${running} -eq 0 ]] && break
  sleep 1
done

###############################
# Removing hosts
###############################
for vhost in $@; do

  virsh undefine ${vhost} || exit 1

  echo "Removing ${vhost} storage volumes"
  rm -fv /var/lib/libvirt/images/${vhost}-*

  echo
  echo "Removing ${vhost} from inventory"
  sed -i "/${vhost}/d" ${inventory}

done

exit 0

