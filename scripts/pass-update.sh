#!/bin/bash

echo "-> Getting passwd from ../ansible/playbooks/provision.yml"
passwd=`cat ../ansible/playbooks/provision.yml \
  | grep vhost_passwd \
  | awk -F'"' '{ print $2 }'`

echo "-> Updating ../packer/centos8/builder.json"
sed -i "s/ssh_password.*/ssh_password\": \"${passwd}\",/g" \
  ../packer/centos8/builder.json 

echo "-> Updating ../packer/centos8/http/centos8-ks.cfg"
sed -i "s/rootpw.*/rootpw ${passwd}/g" \
  ../packer/centos8/http/centos8-ks.cfg

read -p "Start building new OS Base Template images? (Y/n): " yesno
if [[ -n `echo ${yesno} | grep -i y` ]]; then
  ./packer-build.sh
fi


