#!/bin/bash
#
# Author: Kees Remmelzwaal <kees@fastmail.com>
#
#   Provision Virtual Machines with Ansible
#
##############################################################


#######################################
# Default variables
#######################################

_help=0
_verbose=0

#######################################
# Get arguments
#######################################

while getopts "hvg:o:" opt; do
  case ${opt} in
    h) _help=1;;
    v) _verbose=1;;
    g) _group=`echo ${OPTARG} | cut -c-7` ;;
    o) _os=`echo ${OPTARG}    | cut -c-8` ;;
  esac
done

#######################################
# Functions
#######################################

function showHelp() {

  echo
  echo "*******************************"
  echo "* Provisioning                 "
  echo "*******************************"
  echo; showHostgroups
  echo; showSystems
  echo
  echo "*******************************"
  echo
  echo "Usage:"
  echo
  echo "  $0 -g <group> -o <os>"
  echo
  echo "options:"
  echo
  echo " -g <hostgroup>          select hostgroup"
  echo " -o <operating system>   select operating system"
  echo " -h                      show this help message"
  echo " -v                      verbose information"
  echo

  exit 0
}

function showHostgroups() {
  echo "hostgroups:"
  printf " -> "
  for hg in `cat ../ansible/playbooks/deploy.yml \
    | grep 'hosts:' \
    | awk -F':' '{print $2}'`; do
    printf ${hg}" "
  done
  echo
}

function showSystems() {
  echo "operating systems:"
  printf " -> "
  for os in `ls -d ../packer/*/`; do
    os=`echo ${os} | awk -F'/' '{print $3}'`
    printf ${os}" "
  done
  echo
}

#######################################
# Do some checks
#######################################

[[ $_help == 1 ]]  && showHelp
[[ -z ${_os} ]]    && showHelp
[[ -z ${_group} ]] && showHelp

#######################################
# Start script
#######################################

set -x

cd ../ansible/playbooks || exit 1

ansible-playbook provision.yml \
  --extra-vars "hostgroup=${_group} os=${_os}"

exit 0


