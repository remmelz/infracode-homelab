#!/bin/bash

if [[ -z $1 ]]; then
  virsh list
  exit
fi

host=$1
ipaddr=`cat ../ansible/inventory.ini \
  | grep ${host} \
  | awk -F'=' '{print $2}' \
  | awk -F' ' '{print $1}' \
  | tail -1`

ssh -l root ${ipaddr}

