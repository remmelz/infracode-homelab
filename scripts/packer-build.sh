#!/bin/bash

which packer > /dev/null

if [[ $? != 0 ]]; then
  echo "Error: Packer not installed."
  exit 1
fi

echo
echo '***********************************************************'
echo '* To view the progress of installation you need to create *'
echo '* a SSH tunnel and connect with a VNC client:             *'
echo '*                                                         *'
echo '*  ssh -L 5901:localhost:<vnc_port> <buildhost>           *'
echo '***********************************************************'
echo

cd ../packer || exit 1

for os in `ls -d */`; do

  if [[ -n $1  ]]; then
    [[ "$1/" != ${os} ]] && continue
  fi

  cd ./${os} || exit 1
  echo -e "\e[96m[Building ${os}]\e[39m"

  echo "Downloading ISO image.."
  wget -q --show-progress \
    --no-clobber \
    --continue \
    --directory-prefix=./iso \
    `cat iso_source.txt`

  [[ -d ./image ]] && rm -rf ./image

  packer build builder.json

  if [[ -z `ls ./image/ | grep 'qcow2$'` ]]; then
    echo "Error: No base template found in ./packer/image"
    exit 1
  fi

  cd ..

done

exit 0

