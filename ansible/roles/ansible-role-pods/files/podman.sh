#!/bin/bash

[[ $1 == '--start' ]] && action="start"
[[ $1 == '--stop' ]]  && action="stop"
[[ -z ${action} ]]    && exit 1

sleep 10

for container in `podman ps -a \
        | grep -v '^CONTAINER' \
        | awk -F' ' '{print $1}'`; do

  podman ${action} ${container}
done


